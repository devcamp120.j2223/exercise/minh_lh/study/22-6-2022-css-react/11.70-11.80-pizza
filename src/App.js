import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import React, { Component } from 'react';
import pic1 from "./assets/images/1.jpg"
import pic2 from "./assets/images/2.jpg"
import pic3 from "./assets/images/3.jpg"
import pic4 from "./assets/images/4.jpg"
import bacon from "./assets/images/bacon.jpg"
import hawai from "./assets/images/hawaiian.jpg"
import seafood from "./assets/images/seafood.jpg"
import Carousel from 'react-bootstrap/Carousel'

class App extends Component {
  render() {
    return (
      <div>
        {/* header part*/}
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-12">
              <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-warning">
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                  data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                  aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                  <ul className="navbar-nav nav-fill w-100">
                    <li className="nav-item active">
                      <a className="nav-link form-inline" href="#home">Trang chủ</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link form-inline" href="#Menus">Combo</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link form-inline" href="#about">Loại pizza</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link form-inline" href="#contact">Gửi đơn hàng</a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
        {/* slogan part*/}
        <div className="container" id="home" style={{ padding: "120px 0 50px 0" }}>
          <div className="row">
            <div className="col-sm-12">
              <div className="row">
                <div className="col-sm-12">
                  <h1 className="h1Slogan"><b>Pizza 365</b></h1>
                  <h5 className="h5Slogan">Truly italian</h5>
                </div>

                <div className="col-sm-12">
                  <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <Carousel>
                      <Carousel.Item>
                        <img className="d-block w-100" style={{ transform: 'translateX(~100)' }} src={pic1} alt="First slide" />
                      </Carousel.Item>
                      <Carousel.Item>
                        <img className="d-block w-100" style={{ transform: 'translateX(~100)' }} src={pic2} alt="Second slide" />
                      </Carousel.Item>
                      <Carousel.Item>
                        <img className="d-block w-100" style={{ transform: 'translateX(~100)' }} src={pic3} alt="Third slide" />
                      </Carousel.Item>
                      <Carousel.Item>
                        <img className="d-block w-100" style={{ transform: 'translateX(~100)' }} src={pic4} alt="fourth slide" />
                      </Carousel.Item>
                    </Carousel>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* description*/}
          <div className="col-sm-12 text-center p-4 mt-4">
            <h2 style={{ color: "orange" }}><b className="p-2 border-bottom border-warning">Tại sao lại là Pizza 365</b></h2>
          </div>
          <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "lightgoldenrodyellow" }}>
                <h3 className="p-2">Đa dạng</h3>
                <p className="p-2">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hót nhất hiện này.</p>
              </div>
              <div className="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "yellow" }}>
                <h3 className="p-2">Chất lượng</h3>
                <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực
                  phẩn.</p>
              </div>
              <div className="col-sm-3 p-4  border border-warning" style={{ backgroundColor: "lightsalmon" }}>
                <h3 className="p-2">Hương vị</h3>
                <p className="p-2">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</p>
              </div>
              <div className="col-sm-3 p-4  border border-warning" style={{ backgroundColor: "orange" }}>
                <h3 className="p-2">Dịch vụ</h3>
                <p className="p-2">Nhân viên thên thiện, nahf hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân
                  tiến.</p>
              </div>
            </div>
          </div>
          {/*title*/}
          {/* Title what we offer */}
          <div id="Menus" className="row">
            {/* Title what we offer */}
            <div className="col-sm-12 text-center p-4 mt-4">
              <h2><b className="p-1 border-bottom border-warning text-warning">Chọn size pizza</b></h2>
              <p><span className="p-2 text-warning">Chọn combo pizza phù hợp với nhu cầu vủa bạn.</span></p>
            </div>
            {/*  Content what we offer  */}
            <div className="col-sm-12">
              <div className="row">
                <div className="col-sm-4">
                  <div className="card">
                    <div className="card-header text-center" style={{ backgroundColor: "orange" }}>
                      <h3>S(small)</h3>
                    </div>
                    <div className="card-body text-center">
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item"><span>Đường kính: </span><b>20cm</b></li>
                        <li className="list-group-item"><span>Sườn nướng: </span><b>2 pieces</b></li>
                        <li className="list-group-item"><span>Salad: </span><b>200g</b> </li>
                        <li className="list-group-item"><span>Coca Cola: </span><b>2</b> </li>
                        <li className="list-group-item">
                          <h1><b>150.000</b></h1> VND
                        </li>
                      </ul>
                    </div>
                    <div className="card-footer text-center">
                      <button id="btn-SPizza" className="btn btn-warning w-100">Chọn</button>
                    </div>
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="card">
                    <div className="card-header bg-warning text-center">
                      <h3 style={{ backgroundColor: "rgb(255, 196, 0)" }}>M(medium)</h3>
                    </div>
                    <div className="card-body text-center">
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item"><span>Đường kính: </span><b>25 cm</b></li>
                        <li className="list-group-item"><span>Sườn nướng: </span><b>4 pieces</b></li>
                        <li className="list-group-item"><span>Salad: </span><b>300g</b></li>
                        <li className="list-group-item"><span>Coca Cola: </span><b>3</b></li>
                        <li className="list-group-item">
                          <h1><b>200.000</b></h1> VND
                        </li>
                      </ul>
                    </div>
                    <div className="card-footer text-center">
                      <button id="btn-MPizza" className="btn btn-warning w-100">Chọn</button>
                    </div>
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="card">
                    <div className="card-header text-center" style={{ backgroundColor: "orange" }}>
                      <h3>L(large)</h3>
                    </div>
                    <div className="card-body text-center">
                      <ul className="list-group list-group-flush">
                        <li className="list-group-item"> <span>Đường kính: </span><b>30 cm</b></li>
                        <li className="list-group-item"> <span>Sườn nướng: </span><b>8 pieces</b></li>
                        <li className="list-group-item"> <span>Salad: </span><b>500g</b></li>
                        <li className="list-group-item"> <span>Coca Cola: </span><b>4</b></li>
                        <li className="list-group-item">
                          <h1><b>250.000</b></h1> VND
                        </li>
                      </ul>
                    </div>
                    <div className="card-footer text-center">
                      <button id="btn-LPizza" className="btn btn-warning w-100">Chọn</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/*content*/}
          <div className="container">
            <div id="about" className="row">
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom border-warning" style={{ color: "orange" }}>Chọn loại Pizza</b></h2>
              </div>

              <div className="col-sm-12">
                <div className="row">
                  <div className="col-sm-4">
                    <div className="card w-100" style={{ width: "20rem" }}>
                      <img src={seafood} className="card-img-top" alt="seafood" />
                      <div className="card-body">
                        <h4>OCEAN MANIA</h4>
                        <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                        <p>Xốt cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh cua, Hành tây</p>
                        <p><button id="btn-hawaii" className="btn btn-warning w-100">Chọn</button></p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="card w-100" style={{ width: "20rem" }}>
                      <img src={hawai} className="card-img-top" alt="hawaii" />
                      <div className="card-body">
                        <h4>HAWAIIAN</h4>
                        <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                        <p>Xốt cà chua, Phô mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                        <p><button id="btn-haisan" className="btn btn-warning w-100">Chọn</button></p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="card w-100" style={{ width: " 20rem" }}>
                      <img src={bacon} className="card-img-top" alt="hunkhoi" />
                      <div className="card-body">
                        <h4>CHEESY CHICKEN BACON</h4>
                        <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                        <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.</p>
                        <p><button id="btn-hunkhoi" className="btn btn-warning w-100">Chọn</button></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="drinkk" className="col-sm-12 text-center p-4 mt-4">
              <h2 className="p-2" htmlFor="" style={{ color: "orange" }}><b className="border-bottom border-warning">Chọn đồ uống </b></h2>
            </div>
            <div className="col-sm-12">
              <select id="chondouong-inp" className="form-control">
                <option value="none">Tất cả loại nước uống</option>
              </select>
            </div>
            <div id="contact" className="row">
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom border-warning" style={{ color: "orange" }}>Gửi đơn hàng</b></h2>
              </div>

              <div className="col-sm-12 p-2">
                <div className="row">
                  <div className="col-sm-12">
                    <div className="form-group">
                      <label htmlFor="fullname">Tên</label>
                      <input type="text" className="form-control" id="inp-fullname" placeholder="Họ và tên" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Email</label>
                      <input type="text" className="form-control" id="inp-email" placeholder="Email" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="dien-thoai">Số điện thoại</label>
                      <input type="number" className="form-control" id="inp-dien-thoai" placeholder="Điện thoại" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="dia-chi">Địa chỉ</label>
                      <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="message">Mã giảm giá</label>
                      <input type="text" className="form-control" id="inp-ma-giam-gia" placeholder="Mã giảm giá" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="message">Lời nhắn</label>
                      <input type="text" className="form-control" id="inp-loi-nhan" placeholder="Lời nhắn" />
                    </div>
                    <br></br>
                    <button type="button" id="sendOrder-btn" className="btn btn-warning w-100">Gửi</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*footer*/}
        <div className="container-fluid p-5" style={{ backgroundColor: "orange" }}>
          <div className="row text-center">
            <div className="col-sm-12">
              <h4>Footer</h4>
              <a href="/" className="btn btn-dark m-3"><i className="fa fa-arrow-up"></i> To the top</a>
              <div className="m-2">
                <i className="fab fa-facebook-square"></i>
                <i className="fab fa-instagram"></i>
                <i className="fab fa-snapchat"></i>
                <i className="fab fa-pinterest-p"></i>
                <i className="fab fa-twitter"></i>
                <i className="fab fa-linkedin-in"></i>
              </div>
              <div>
                <h5 className="m-2">Powered by DEVCAMP</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
